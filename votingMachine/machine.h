#ifndef MACHINE_H
#define MACHINE_H

#include <QIODevice>
#include <QObject>
#include <QList>
#include <QFile>
#include <QString>

#include "riding.h"

class Machine : public QObject
{
    Q_OBJECT
public:
    explicit Machine( QString dataLocation, QObject *parent = 0 );
    // Populate riding and Candidates. calls addRiding and then Candidate::create___()
    // Call this in the constructor
    bool populateRidings();
    Riding* addRiding();
    QList<Riding *> ridingsList() const;

private:
    QList<Riding*> m_ridingsList;
    QString m_dataLocation;

signals:

public slots:
};

#endif // MACHINE_H
