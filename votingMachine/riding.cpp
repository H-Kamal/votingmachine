#include "riding.h"

Riding::Riding(QObject *parent) : QObject(parent)
{

}

QList<Candidate*> Riding::getCandidates()
{
    return m_candidatesList;
}

// For each riding, will add the to the candidates list
Candidate* Riding::addCandidate()
{
    Candidate* newCandidate = new Candidate();
    if (newCandidate != nullptr)
    {
        m_candidatesList.append(newCandidate);
    }
    return newCandidate;
}

void Riding::setName(QString &name)
{
    if (name != nullptr)
    {
        m_ridingName = name;
    }
}

QString Riding::getRidingName() const
{
    return m_ridingName;
}
