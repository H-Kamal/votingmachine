#ifndef MACHINECONTROLLER_H
#define MACHINECONTROLLER_H
#include "machine.h"

#include <QObject>
#include <fstream>

class MachineController : public QObject
{
    Q_OBJECT
public:
    explicit MachineController( Machine* machine, QObject *parent = 0 );
    Machine *machine() const;

private:
    Machine* m_machine;
signals:

public slots:
};

#endif // MACHINECONTROLLER_H
