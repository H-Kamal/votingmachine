#include "machine.h"

Machine::Machine(QString dataLocation, QObject *parent) : m_dataLocation(dataLocation)
{
    populateRidings();
}

bool Machine::populateRidings()
{
    QFile dataFile(m_dataLocation);
    if (!dataFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        return false;
    }

    QString ridingNumber("");
    Riding* currentRiding = nullptr;
    Candidate* currentCandidate = nullptr;
    while (!dataFile.atEnd())
    {
        QString line = dataFile.readLine();
        QList<QString> tokenizedList = line.split(',');
        // If the current line's riding number is not the same as the last, create new riding
        if (tokenizedList[0] != ridingNumber)
        {
            // Create a new riding and set its name
            currentRiding = addRiding();
            currentRiding->setName(tokenizedList[1]);
            ridingNumber = tokenizedList[0];
        }
        // Always read the line's candidate and add their name and party to Candidate object
        currentCandidate = currentRiding->addCandidate();
        currentCandidate->setName(tokenizedList[6] += QString(" ") += tokenizedList[5]);
        currentCandidate->setParty(tokenizedList[3]);
    }
    return true;
}

Riding* Machine::addRiding()
{
    Riding* newRiding = new Riding();
    if (newRiding != nullptr)
    {
        m_ridingsList.append(newRiding);
    }
    return newRiding;
}

QList<Riding *> Machine::ridingsList() const
{
    return m_ridingsList;
}

