#include "candidate.h"

Candidate::Candidate(QObject *parent) : QObject(parent), m_votes(0)
{

}

QString Candidate::name()
{
    return m_name;
}

QString Candidate::party()
{
    return m_party;
}

unsigned int Candidate::votes()
{
    return m_votes;
}

//To Do: Grab the name from a central source automatically
void Candidate::setName(QString &name)
{
    if ( name != nullptr )
    {
        m_name = name;
        emit nameChanged();
    }
}
//To Do: Grab the party from a central source automatically
void Candidate::setParty(QString &party)
{
    if ( party != nullptr )
    {
        m_party = party;
        emit partyChanged();
    }
}

bool Candidate::incrementVote()
{
    if ( (m_votes + 1) < UINT_MAX )
    {
        m_votes++;
        emit voteIncremented();
        return true;
    }
    return false;
}
