#include "machinecontroller.h"

MachineController::MachineController(Machine *machine, QObject *parent) : QObject(parent), m_machine(machine)
{

}

Machine *MachineController::machine() const
{
    return m_machine;
}
