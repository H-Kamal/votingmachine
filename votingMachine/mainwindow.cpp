#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(MachineController* controller, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_controller(controller)
{
    ui->setupUi(this);
    setUpConnections();
    addRidingstoList();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::displayRidings()
{
    // Changes the page to the list of ridings
    ui->stackedWidget->setCurrentWidget(ui->changeRiding);
}

// Choose the riding from list and populate candidate buttons as required
void MainWindow::changeRiding()
{
    QListWidgetItem* chosenItem = ui->ridingsList->currentItem();
    if (chosenItem)
    {
        // Create the widget which will be the stacked widget page with all the candidates
        choicePage = new QWidget();
        choicePage->setObjectName(QStringLiteral("choicePage"));

        // Create a new vertical layout with the parent being the choicePage widget
        choiceVerticalLayout = new QVBoxLayout(choicePage);
        choiceVerticalLayout->setSpacing(6);
        choiceVerticalLayout->setContentsMargins(11, 11, 11, 11);
        choiceVerticalLayout->setObjectName(QStringLiteral("choiceVerticalLayout"));

        // Look up riding associated with list item in map
        Riding* currentRiding = m_ridingsMap.value(chosenItem);
        // Load the correct candidates on the page
        QList<Candidate*> candidatesList = currentRiding->getCandidates();
        QList<Candidate*>::iterator itr;
        for (itr = candidatesList.begin(); itr != candidatesList.end(); ++itr)
        {
            // Create a push button with the parent being the choicePage widget
            candidateButton = createNamedButton((*itr)->name() += QString("\n") += (*itr)->party());
            m_candidateButtonMap.insert(*itr, candidateButton);
            // Add the candidate button to the vertical layout
            choiceVerticalLayout->addWidget(candidateButton);
            // Add a slot connecting the candidate button on UI to candidateButtonClicked which will increment right vote
            connect(candidateButton, &QPushButton::clicked, this, &MainWindow::candidateButtonClicked);
        }

        // Add the page widget as a page in the stacked widget
        ui->stackedWidget->addWidget(choicePage);
        ui->stackedWidget->setCurrentWidget(choicePage);
    }
}

void MainWindow::addRidingstoList()
{
    QListWidgetItem* ridingItem;

    QList<Riding*> ridingsList = m_controller->machine()->ridingsList();
    QList<Riding*>::iterator itr;
    for (itr = ridingsList.begin(); itr != ridingsList.end(); ++itr)
    {
        //Dynamically adding a new list item with the current riding's name
        ui->ridingsList->addItem((*itr)->getRidingName());
        // We populate the m_RidingsMap map so we can correspond each Riding to each listWidget item
        ridingItem = ui->ridingsList->item(ui->ridingsList->count() - 1);
        m_ridingsMap.insert(ridingItem, *itr);
    }
}

QPushButton* MainWindow::createNamedButton(QString &name)
{
    QPushButton* button = new QPushButton(name);
    button->setObjectName(QString("button_") += QString("name"));
    return button;
}

void MainWindow::candidateButtonClicked()
{
    // Will give the pointer to the object which called the slot
    QObject* senderButton = sender();
    // Find the candidate* key from map based on the QPushButton*
    Candidate* incrementCandidate = m_candidateButtonMap.key(qobject_cast<QPushButton*>(senderButton));
    // Confirmation dialog box
    QMessageBox msgBox;
    msgBox.setText(QString("Confirm:"));
    msgBox.setInformativeText(QString("Do you want to vote for ") += QString("<b>") += incrementCandidate->name() += QString("</b>"));
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    int confirmRet = msgBox.exec();
    switch (confirmRet){
        case QMessageBox::Yes:
            incrementCandidate->incrementVote();
            qDebug() << "Incremented the vote of " << incrementCandidate->name() << " to " << incrementCandidate->votes();
            break;
        case QMessageBox::No:
            qDebug() << "Did not increment the vote of " << incrementCandidate->name();
            break;
    }
}

void MainWindow::setUpConnections()
{
    connect(ui->actionChange_Riding, &QAction::triggered, this, &MainWindow::displayRidings);
    connect(ui->selectButton, &QPushButton::clicked, this, &MainWindow::changeRiding);
}
