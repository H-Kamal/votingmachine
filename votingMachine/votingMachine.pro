#-------------------------------------------------
#
# Project created by QtCreator 2019-12-19T23:05:50
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = votingMachine
TEMPLATE = app
CONFIG += c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    candidate.cpp \
    riding.cpp \
    machine.cpp \
    machinecontroller.cpp

HEADERS  += mainwindow.h \
    candidate.h \
    riding.h \
    machine.h \
    machinecontroller.h

FORMS    += mainwindow.ui

RESOURCES += \
    resources.qrc
