#ifndef CANDIDATE_H
#define CANDIDATE_H

#include <QObject>
#include <QString>

#include <limits>

class Candidate : public QObject
{
    Q_OBJECT
public:
    explicit Candidate(QObject *parent = 0);
    QString name();
    QString party();
    unsigned int votes();
    void setName(QString &name);
    void setParty(QString &party);
    bool incrementVote();

private:
    QString m_name;
    QString m_party;
    unsigned int m_votes;

signals:
    void nameChanged();
    void partyChanged();
    // Should connect to the view randomly changing the order of candidates
    void voteIncremented();

public slots:
};

#endif // CANDIDATE_H
