#ifndef RIDING_H
#define RIDING_H

#include <QObject>
#include <QList>

#include "candidate.h"

class Riding : public QObject
{
    Q_OBJECT
public:
    explicit Riding(QObject *parent = 0);
    QList<Candidate*> getCandidates();
    Candidate* addCandidate();
    void setName(QString &name);
    QString getRidingName() const;

signals:

private:
    QList<Candidate*> m_candidatesList;
    QString m_ridingName;
public slots:
};

#endif // RIDING_H
