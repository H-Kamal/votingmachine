#include "mainwindow.h"
#include "machinecontroller.h"
#include "machine.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QString dataLocation = ":/text/candidates.txt";
    Machine machine(dataLocation);
    MachineController controller(&machine);

    MainWindow w(&controller);
    w.show();

    return a.exec();
}
