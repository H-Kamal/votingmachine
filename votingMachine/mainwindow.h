#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidget>
#include <QBoxLayout>
#include <QPushButton>
#include <QDebug>
#include <QMessageBox>
#include "machinecontroller.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow( MachineController* controller, QWidget *parent = 0);
    ~MainWindow();
    void setUpConnections();
    void addRidingstoList();
    QPushButton* createNamedButton(QString &name);

private:
    Ui::MainWindow *ui;
    MachineController* m_controller;
    QHash<QListWidgetItem*, Riding*> m_ridingsMap;
    QHash<Candidate*, QPushButton*> m_candidateButtonMap;

    QWidget * choicePage;
    QVBoxLayout* choiceVerticalLayout;
    QPushButton* candidateButton;
    QList<QPushButton*> buttonsList;


public slots:
    void displayRidings();
    void changeRiding();
    void candidateButtonClicked();
};

#endif // MAINWINDOW_H
